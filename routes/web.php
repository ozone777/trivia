<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
	
	if(\Auth::check()){
		return redirect("/competition");
	}else{
		return redirect("/register");
	}
	
});

Route::get('/admin', function (){
    return redirect("/questions");
});

Route::resource("tweets","TweetController");
Route::resource("offices","OfficeController"); 
Route::get('/search_office', 'OfficeController@search_office');
Route::resource("answers","AnswerController"); 
Route::resource("questions","QuestionController"); 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/help', 'HomeController@help');
Route::get('/test', 'HomeController@test')->name('test');
Route::get('/competition', 'CompetitionController@competition')->name('competition');
Route::post('/send_answer', 'CompetitionController@send_answer');
Route::get('/finish', 'CompetitionController@finish');
Route::get('/final_result', 'CompetitionController@final_result');
Route::get('/top', 'CompetitionController@top');

Route::get('/list_users', 'AdminController@list_users');
Route::get('/search_user', 'AdminController@search_user');
Route::get('/user_detail/{id}', 'AdminController@user_detail');
