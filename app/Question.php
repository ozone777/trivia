<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Answer;
use App\Result;
use DB;

class Question extends Model
{
	//
	public function answers()
	{
		return $this->hasMany('App\Answer');
	}
	
	public function results()
	{
		return $this->hasMany('App\Result');
	}
	
	public function answer_letters(){
		return ["a","b","c","d"];
	}
	
	public function get_valid_answer(){
		
		$answer = Answer::where("question_id",$this->id)->where('valid', true)->first();
		return $answer;
		
	}
	
	public function get_valid_answer_id(){
		
		$answer = Answer::where("question_id",$this->id)->where('valid', true)->first();
		if(isset($answer)){
			return $answer->id;
		}else{
			return 0;
		}
		
	}
	
	public function get_valid_answer_ids(){
		
		$answers = Answer::orderBy('id', 'desc')->where("question_id",$this->id)->where('valid', true)->get();
		$answers_ids = $answers->pluck('id')->toArray();
		return $answers_ids;
		
	}
	
	public function rate($user_id,$user_answer_id,$final_answer=null,$answer_content=null){
		
		//Log::info("Final question: $final_answer");
		
		$result = Result::get_result($user_id,$this->id);
		
		if($this->type == "complete"){
			$result->answer_ids = $final_answer;
			if($this->winning_combination == $final_answer){
				$result->valid = true;
			}else{
				$result->valid = false;
			}
		}else if($this->type =="yes_or_no"){
			
			//Compare answers
			$result = Result::get_result($user_id,$this->id);
			$result->answer_id = $user_answer_id;
			if($user_answer_id == $this->get_valid_answer_id()){
				$result->valid = true;
			}else{
				$result->valid = false;
			}
			$result->content = $answer_content;
			$result->answer_id = $user_answer_id;
			
		}else if($this->type =="multiple_choice"){
			//Compare answers
			$result = Result::get_result($user_id,$this->id);
			$result->answer_id = $user_answer_id;
			if($user_answer_id == $this->get_valid_answer_id()){
				$result->valid = true;
			}else{
				$result->valid = false;
			}
			
		}else if($this->type =="multiple_choices"){
			//Get valid answers ids
			$valid_answers = $this->get_valid_answer_ids();
			//Compare answers
			$result = Result::get_result($user_id,$this->id);
			$result->answer_ids = $user_answer_id;
			$user_answers = explode(",", $user_answer_id);

			//sort($user_answers);
			sort($valid_answers);
			
			Log::info("valid_answers:");
			Log::info($valid_answers);
			
			Log::info("user_answers:");
			Log::info($user_answers);
			
			$user_answers = array_unique($user_answers);
			sort($user_answers);
			
			Log::info("user_answers despues de array_unique:");
			Log::info($user_answers);
			
			if($user_answers == $valid_answers){
				$result->valid = true;
			}else{
				$result->valid = false;
			}
		}else if($this->type =="complete_with_content"){
			$result = Result::get_result($user_id,$this->id);
			$result->content = $answer_content;
			$result->valid = true;
		}
		
		$result->save();
		return $result;
	}
	
	
	/*
			  <option value="complete">complete</option>
			  <option value="yes_or_no">yes_or_no</option>
			  <option value="multiple_choices">multiple_choices</option>
			  <option value="complete_with_content">complete_with_content</option>
	*/
	
	public function is_complete(){
		if($this->type == "complete"){
			return true;
		}else{
			return false;
		}
	}
	
	public function is_yes_or_no(){
		if($this->type == "yes_or_no"){
			return true;
		}else{
			return false;
		}
	}
	
	public function is_multiple_choice(){
		if($this->type == "multiple_choice"){
			return true;
		}else{
			return false;
		}
	}
	
	public function is_multiple_choices(){
		if($this->type == "multiple_choices"){
			return true;
		}else{
			return false;
		}
	}
	
	public function is_complete_with_content(){
		if($this->type == "complete_with_content"){
			return true;
		}else{
			return false;
		}
	}
	
	public static function get_last_answered_question($user_id){
		$question = DB::table('questions')
		                     ->select(DB::raw('questions.id, results.valid, results.user_id'))
							 ->join('results', 'questions.id', '=', 'results.question_id')
							 ->where("user_id",$user_id)
							 ->orderBy('id', 'desc')
							 ->get()
							 ->first();
		return $question;
	}
	
	
}
