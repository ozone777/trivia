<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\Office;
use App\Question;
use App\Result;
use App\User;
use Illuminate\Support\Facades\Log;
use Faker\Factory as Faker;
use Exception;

class CreateAdmin extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	
	//Correr en modo production Esta
	
	protected $signature = 'create_admin';
	#protected $signature = 'addfields {--queue=}';
	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Create admin';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	
	
	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle()
	{
		
		//$user = new User();
		
		$user = User::where("email","iclaussen@milpagroup.com")->first();
		
		if(isset($user)){
			$user->delete();
		}
		
		$user = new User();
		$user->name = "admin";
		$user->email = "iclaussen@milpagroup.com";
		$user->password = bcrypt('milpa777');
		$user->admin = true;
		$user->save();	
		
	}
}
