<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\Office;
use App\Question;
use App\Result;
use App\User;
use Illuminate\Support\Facades\Log;
use Faker\Factory as Faker;
use Exception;

class ResetAll extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	
	//Correr en modo production Esta
	//php artisan fake_users --u 3000
	
	protected $signature = 'reset_all';
	#protected $signature = 'addfields {--queue=}';
	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Reset all';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	
	
	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle()
	{
		
		DB::table('answers')->delete();
		DB::table('offices')->delete();
		
		DB::table('questions')->delete();
		DB::table('ratings')->delete();
		
		DB::table('results')->delete();
		DB::table('users')->delete();
		
	}
}
