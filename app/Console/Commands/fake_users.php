<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\Office;
use App\Question;
use App\Result;
use App\User;
use Illuminate\Support\Facades\Log;
use Faker\Factory as Faker;
use Exception;

class FakeUsers extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	
	//Correr en modo production Esta
	//php artisan fake_users --u 3000
	
	protected $signature = 'fake_users {--u=}';
	#protected $signature = 'addfields {--queue=}';
	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Fake users';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	
	
	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle()
	{
		DB::table('users')->delete();
		DB::table('results')->delete();
		DB::table('ratings')->delete();
		
		$number_of_users = $this->option('u');		
		$offices = Office::orderBy('id', 'desc')->get();
	    
		foreach (range(1,$number_of_users) as $index) {
			
			$faker = Faker::create();
			$random_index = rand(1,($offices->count()-1));
			$office = $offices[$random_index];
			$employee_number = rand(11111,111111);
			
			$user = new User();
			
			/*
			DB::table('users')->insert([
				'name' => $faker->name,
				'email' => $faker->email,
				'password' => bcrypt('secret'),
				'office_id' => $office->id,
				'employee_number' => $employee_number
				]);
			*/
			$user->name = $faker->name;
			$user->email = $faker->email;
			$user->password =  bcrypt('secret');
			$user->office_id = $office->id;
			$user->employee_number = $employee_number;
			
			try {
			    
			    $user->save();

			} catch (Exception $e) {
			   $this->info("error in create user");
			}
			
			
		}
		
		//Fake Results
		$questions = Question::orderBy('id', 'desc')->get();
		$users = User::orderBy('id', 'desc')->get();
		
		Log::info("FAKE USERS GENERATOR");
		
		foreach ($users as $user) {
			
			
			$good_answers = rand(1,9);
			$bad_answers = $questions->count() - $good_answers;
			$bad_answers_init = $questions->count() - $bad_answers;
			$bad_answers_init++;
			
			Log::info("good_answers : $good_answers");
			Log::info("bad_answers : $bad_answers");
			Log::info("bad_answers_init : $bad_answers_init");
			Log::info("-------------------------");
			
			for ($x = 0; $x <= $good_answers; $x++) {
				$question = $questions[$x];
				$result = new Result();
				$result->question_id = $question->id;
			    $result->valid = true;
				$result->user_id = $user->id;
				$result->save();
			} 
			
			for ($j = $bad_answers_init; $j < $questions->count(); $j++) {
				$question = $questions[$j];
				$result = new Result();
				$result->question_id = $question->id;
			    $result->valid = false;
				$result->user_id = $user->id;
				$result->save();
			}
			
			$user->calculate_test_rating();
			
		}
		
	}
}
