<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Result;
use App\Rating;

class User extends Authenticatable
{
    use Notifiable;
	
	public function results()
	{
		return $this->hasMany('App\Result');
	}
	
	public function office()
	{
	    return $this->belongsTo('App\Office');
	}
	
	public function current_question_id(){
		$result = Result::where("user_id",$this->id)->orderBy('id', 'desc')->first();
		if(isset($result)){
			return $result->question_id+1;
		}else{
			return 1;
		}
		
	}
	
	
	public function calculate_test_rating(){
		
		$rating = $this->get_rating();
		$cont = 0;
		$results_array = Result::where("user_id",$this->id)->get();
		foreach($results_array as $r){
			if($r->valid){
				$cont++;
			}
		}
		$rating->rating = $cont;
		$rating->save();
	}
	
	public function get_rating(){
		$rating = Rating::where("user_id",$this->id)->first();
		if(isset($rating)){
			return $rating;
		}else{
			$rating = new Rating();
			$rating->user_id = $this->id;
			return $rating;
		}
	}
	
	public function get_test_rating(){
		$rating = Rating::where("user_id",$this->id)->first();
		return $rating->rating;
	}
	
	public function admin(){
		return $this->admin;
	}
	

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','employee_number','office_id', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
}
