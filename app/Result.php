<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	
	public function user()
	{
	        return $this->belongsTo('App\User');
	}
	
	public function question()
	{
	        return $this->belongsTo('App\Question');
	}
	
    //
	public static function get_result($user_id,$question_id){
		$result = Result::where("question_id",$question_id)->where("user_id",$user_id)->first();
		if(isset($result)){
			return $result;
		}else{
			$result = new Result();
			$result->user_id = $user_id;
			$result->question_id = $question_id;
			return $result;
		}
	}
	
	public static function exists_result($user_id,$question_id){
		$result = Result::where("question_id",$question_id)->where("user_id",$user_id)->first();
		if($result){
			return true;
		}else{
			return false;
		}
	}
	
	public function result_label(){
		if($this->valid){
			return "correcta";
		}else{
			return "incorrecta";
		}
	}
	
	
}
