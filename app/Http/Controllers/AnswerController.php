<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Answer;
use App\Question;
use Illuminate\Http\Request;

class AnswerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	    View::share('viewsw', 'answers'); 
		$this->middleware('auth.admin');
	}
	
	public function index()
	{
		$answers = Answer::orderBy('id', 'desc')->paginate(10);

		return view('answers.index', compact('answers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$questions = Question::orderBy('id', 'asc')->get();
		return view('answers.create',compact('questions'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$answer = new Answer();

		$answer->content = $request->input("content");
        $answer->question_id = $request->input("question_id");
        $answer->valid = $request->input("valid");

		$answer->save();

		return redirect()->route('questions.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$answer = Answer::findOrFail($id);
		
		return view('answers.show', compact('answer'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$questions = Question::orderBy('id', 'asc')->get();
		$answer = Answer::findOrFail($id);

		return view('answers.edit', compact('answer','questions'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$answer = Answer::findOrFail($id);

		$answer->content = $request->input("content");
        $answer->question_id = $request->input("question_id");
        $answer->valid = $request->input("valid");

		$answer->save();

		return redirect()->route('questions.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$answer = Answer::findOrFail($id);
		$answer->delete();

		return redirect()->route('questions.index')->with('message', 'Item created successfully.');
	}

}
