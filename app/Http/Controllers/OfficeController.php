<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Office;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;

class OfficeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	    View::share('viewsw', 'offices'); 
		$this->middleware('auth.admin');
	}
	
	public function index()
	{
		//$offices = Office::orderBy('id', 'desc')->paginate(50);
		/*

		
		SELECT o.id, o.members, o.name, AVG(r.rating) AS AverageRating FROM users INNER JOIN offices as o ON o.id = users.office_id INNER JOIN ratings as r ON r.user_id = users.id GROUP BY o.id
		
		*/
		
		$offices = DB::table('users')
		                     ->select(DB::raw('offices.id, offices.members, offices.name, AVG(ratings.rating) AS average, Count(users.id) as answered'))
							 ->join('offices', 'offices.id', '=', 'users.office_id')
							 ->join('ratings', 'ratings.user_id', '=', 'users.id')
							 ->groupBy('offices.id')
							 ->orderBy('average', 'desc')
							 ->get();
		
		return view('offices.index', compact('offices'));
	}
	
	public function search_office(){
		
		$param = Input::get('param');
		$offices = DB::table('users')
		                     ->select(DB::raw('offices.id, offices.members, offices.name, AVG(ratings.rating) AS average, Count(users.id) as answered'))
							 ->join('offices', 'offices.id', '=', 'users.office_id')
							 ->join('ratings', 'ratings.user_id', '=', 'users.id')
							 ->groupBy('id')
							 ->where("offices.name","like","%".$param."%")
							 ->orderBy('average', 'desc')
							 ->get();
		
		return view('offices.search_office', compact('offices'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('offices.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$office = new Office();

		$office->members = $request->input("members");
        $office->name = $request->input("name");

		$office->save();

		return redirect()->route('offices.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$office = Office::findOrFail($id);
		$users = $office->users()->get();
		return view('offices.show', compact('office','users'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$office = Office::findOrFail($id);
		return view('offices.edit', compact('office'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$office = Office::findOrFail($id);

		$office->members = $request->input("members");
        $office->name = $request->input("name");

		$office->save();

		return redirect()->route('offices.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$office = Office::findOrFail($id);
		$office->delete();

		return redirect()->route('offices.index')->with('message', 'Item deleted successfully.');
	}

}
