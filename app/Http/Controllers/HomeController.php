<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	
    public function help()
    {
		$click = Input::get('click');
		$question_id = Input::get('question_id');
		
        return view('home.help',compact('question_id','click'));
		
    }
	
    public function test()
    {
        return view('test');
    }
	
}
