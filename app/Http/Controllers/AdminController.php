<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;
use DB; 
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
		$this->middleware('auth.admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_users()
    {
		$users = User::orderBy('users.id', 'asc')
			    ->select(DB::raw('users.id, users.employee_number, users.name, offices.name as office_name, ratings.rating'))
				->join('offices', 'offices.id', '=', 'users.office_id')
				->join('ratings', 'ratings.user_id', '=', 'users.id')
				->paginate(30);
		
		$viewsw = "users";
		
        return view('admin.list_users',compact('users','viewsw'));
    }
	
    public function search_user()
    {
		/*
		$param = Input::get('param');
		$users = User::orderBy('id', 'asc')->where("name","like","%".$param."%")->get();
		$viewsw = "users";
		*/
		
		$param = Input::get('param');
		$viewsw = "users";
		
		$users = User::orderBy('users.id', 'asc')
			    ->select(DB::raw('users.id, users.employee_number, users.name, offices.name as office_name, ratings.rating'))
				->join('offices', 'offices.id', '=', 'users.office_id')
				->join('ratings', 'ratings.user_id', '=', 'users.id')
				->where("users.name","like","%".$param."%")
				->get();
				
		
        return view('admin.search_user',compact('users','viewsw','param'));
    }
	
    public function user_detail($id)
    {
		$user = User::findOrFail($id);
		$viewsw = "users";
		$results = $user->results()->orderBy('question_id', 'asc')->get();
        return view('admin.user_detail',compact('user','viewsw','results'));
    }
	
}
