<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Result;
use App\Rating;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use DB;

class CompetitionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.custom');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function competition()
    {
		Log::info("Entro en competition");
		
		$current_user = Auth::user();		
		$question_id = $current_user->current_question_id();
		
		Log::info("user_id: $current_user->id");
		Log::info("question_id: $question_id");
		
		$question = Question::find($question_id);
		Log::info("Despues de question-query");
		if(isset($question)){
			
			$next_question = $question->id+1;
			$competition_view ="questions";
			
			if($question->is_yes_or_no() || $question->is_complete()){
				$answers = $question->answers()->get();
			}else{
			$answers = $question->answers()->get()->shuffle();
			}
		
			Log::info("antes de result");
			$result = Result::where("user_id",$current_user->id)->where("question_id",$question->id)->first();	
		}else{
			Log::info("antes de top");
			return redirect("/final_result");
		}
		
		$cont = 0;
        return view('competition.competition',compact('question','competition_view','answers','next_question','cont','result','click','question_id','current_user'));
    }
	
	public function top(){
		$question_id = Input::get('question');
		$current_user = Auth::user();	
		$final = Input::get('final');
		$offices = DB::table('users')
		                     ->select(DB::raw('offices.id, offices.members, offices.name, AVG(ratings.rating) AS average'))
							 ->join('offices', 'offices.id', '=', 'users.office_id')
							 ->join('ratings', 'ratings.user_id', '=', 'users.id')
							 ->groupBy('id')
							 ->orderBy('average', 'desc')
							 ->get();
		
		Log::info("offices query:");
		Log::info($offices);
		
		
	   return view('competition.top',compact('offices','current_user','question_id','final'));
		
	}
	
	public function send_answer(){
		
		$current_user = Auth::user();		
		$question_id = Input::get('question_id');
		$final_answer = Input::get('final_answer');
		$user_answer_id = Input::get('answer_id');
		$answer_content = Input::get('answer_content');
		
		$question = Question::findOrFail($question_id);
		$questions_count = Question::orderBy('id', 'desc')->count();
		
		//LOG
		Log::info("DEBUG");
		Log::info("user_id: $current_user->id");
		Log::info("question_id: $question_id");
		Log::info("final_answer: $final_answer");
		Log::info("user_answer_id: $user_answer_id");
		Log::info("answer_content: $answer_content");
		
		if(Result::exists_result($current_user->id,$question->id)){
			return response()->json(["message" => "protection"]);
		}
		
		$result = $question->rate($current_user->id,$user_answer_id,$final_answer,$answer_content);
		
		if($question->id == $questions_count){
		   $current_user->calculate_test_rating();
		}
		
		return response()->json(["valid" => $result->valid, "question_id" => $question->id ,"question" => $question, "questions_count" => $questions_count]);
		
	}
	
	public function finish(){
		
		return view('competition.finish');
	}
	
	public function final_result(){
		$current_user = Auth::user();
		$rating = Rating::where("user_id",$current_user->id)->first();
		return view('competition.final_result',compact('rating'));
	}
	
}
