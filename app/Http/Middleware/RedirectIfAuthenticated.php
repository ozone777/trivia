<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Log;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }
		
		/*
		Log::info($request->route()->getActionName());
		$temproute = $request->route()->getActionName();
		//Log::info(print_r($request->route(), true));
		
		if (!Auth::check() & ($temproute != "App\Http\Controllers\Auth\RegisterController@showRegistrationForm") & ($temproute != "App\Http\Controllers\Auth\LoginController@showLoginForm")) {
		     return redirect('/register'); // redirect to your specific page which is public for all
		}
		*/
		

        return $next($request);
    }
}
