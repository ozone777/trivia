<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB; 

class Office extends Model
{
    //
	public function users()
	{
		return $this->hasMany('App\User');
	}
	
	public function get_rating_info($field){
		$office = DB::table('users')
		                     ->select(DB::raw('offices.id, offices.members, offices.name, AVG(ratings.rating) AS average, Count(users.id) as answered'))
							 ->join('offices', 'offices.id', '=', 'users.office_id')
							 ->join('ratings', 'ratings.user_id', '=', 'users.id')
							 ->where("offices.id",$this->id)
							 ->groupBy('id')
							 ->orderBy('average', 'desc')
							 ->get()
							 ->first();
		
		if(isset($office)){
			if($field == "average"){
				return $office->average;
			}else if($field == "answered"){
				return $office->answered;
			}
		}
		
	}
	
}
