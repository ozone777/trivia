<div id = "question_area">
	
	<div class="questions">

	<div class="row">
		<div class="col">
			<div class="text-center">
				<h1 class="big_title">0{{$question->id}}</h1>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col">
			<div class="text-center">
				<h2 class="medium_title">ELIGE LA RESPUESTA CORRECTA</h3>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="text-center">
				<img src="/images/icon{{$question->id}}.png" style="max-width: 40px; margin: 10px 10px 10px 10px;">
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col">
			<div class="text-center">
				<div id="question_content">
					 
					 <p>{!!$question->content!!}</p>
					 
				</div>
			</div>
		</div>
	</div>

	<div id="answer_area">
		
	<input type="hidden" id="answer_id" name="answer_id" value="">
	
	@foreach($answers as $answer)
		<div class="row">
			<div class="col">
				@if($answer->valid)
				
				<p class="multiple_choice_button trueone" answer_id ="{{$answer->id}}"><b>{{$question->answer_letters()[$cont]}}.</b> {{$answer->content}}</p>
				
				@else
				
				<p class="multiple_choice_button falseone" answer_id ="{{$answer->id}}"><b>{{$question->answer_letters()[$cont]}}.</b> {{$answer->content}}</p>
				
				@endif
		</div>
		</div>
	<?php
	$cont++;
	?>
	@endforeach
	
	</div>
	
	
	<div class="row">
	
			
		<div class="col">
					<div class="text-center">
	             <button type="submit" id="send_multiple_choice_answer" class="myButton">
	                        Validar
	             </button>
				 	</div>
				</div>
	
	</div>
	
	<div class="row">	
		<div class="col">
					<div class="text-center">
						
						<div class="pages_info">{{$question->id}} / 10</div>
	
				 	</div>
				</div>
	
	</div>

</div>

</div>

