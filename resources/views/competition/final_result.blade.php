<?php
	
$offices = \App\Office::orderBy('id', 'desc')->get();
$uniqid = uniqid();
	
?>

<!DOCTYPE html>
<!-- saved from url=(0051)https://getbootstrap.com/docs/4.1/examples/navbars/ -->
<html lang="en"><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/prompt.js"></script><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/runScript.js"></script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Concurso Super 99</title>

	<link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/navbars/">

	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="/css/navbar.css" rel="stylesheet">
	<link href="/css/quiz.css" rel="stylesheet">
	
	<!-- Latest compiled and minified CSS -->

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
</head>

<style>

	@media (max-width: 576px) { 

		img{
			max-height: 510px;
			}
	}
	
    @media (min-width: 577px) { 
		img{
			max-height: 510px;
			}
    }
 
    @media (min-width: 768px) { 
		img{
			max-height: 510px;
			}
     }
	 
	 /* Centered text */
	 .centered {
	   position: absolute;
	   top: 50%;
	   left: 50%;
	   transform: translate(-50%, -50%);
	 }
	 
	 .centered h1{
	 	font-family: "Gotham Rounded";
	 	font-weight: bold;
	 	font-size: 4em;
	 	letter-spacing: 0.08em;
	 	line-height: 1em;
	 	text-align: center;
	 	color: #f5333f;
	 	padding: 0px 0px 0px 0px;
		margin: 0px 0px 0px 0px;
		display: inline-block;
	 }
	 
	 .green h1{
		 color: #6D9B45;
	 }
	 
	 .red h1{
		 text-color: #F5333F;
	 }
	 
	 .blue h1{
		 text-color: #0D96D3;
	 }
	
</style>

<body>
	
	
	@include('competition/_navbar2')
	
<div class="container">
		
	<div class="row">
		<div class="col-12">
			<div class="text-center">
					<img src="/images/final_blank.png">
					<div class ="centered">
						
						<div style="margin-bottom: 155px" id="final_title">
							<h1>TU</h1>
							<h2>PUNTAJE</h2>
							<br />
							@if(isset($rating))
							<h1 style="color: #6D9B45; font-size: 3.5em;">{{$rating->rating}}</h1><h1 style="color: #0D96D3; font-size: 3.5em;">/</h1><h1 style="color: #F5333F; font-size: 3.5em;">10</h1>
							@endif
							
						</div>
						
						
			
					</div>
		
			</div>
		</div>
	</div>
	
	<!-- 
	<div class="row">
		<div class="col-12">
	
	<button id="btn1">Animate height</button>
	<button id="btn2">Reset height</button>

	<div id="container_p" style="height: 500px">

		<div id="box" style="background:#98bf21;height:100px;width:100px;margin:6px; position: absolute;  bottom: 0;"></div>
	
	   
		</div>
		
	</div>
		
	</div>
	
	-->
	
</div>

<footer class="footer mt-auto py-3" style="background-color: #F5333F">
  <div class="container">
	  <div class="text-center">
		  <div class="footer_text">
    	  <a href="/top?final=true"><h2>Revisa si tu equipo ya <br /> está en el <strong>TOP 3</strong><h2></a>
		  </div>
	  </div>
  </div>
</footer>



<script>
	
	$( document ).ready(function() {
		
		function set_message(){
		  //alert("Boom!");
		  html="";
		  
		  @if(isset($rating))
		    @if($rating->rating >6)
		  		html+='<img style="width: 170px; height: auto" src="/images/eres_super.png">';
				html+='<br />';
		  	  	html+='<img src="/images/good_rating_text.png">';
		  	  	$("#final_title").css("margin-bottom", "30px");
		  	  	$("#final_title").html(html);
			@else
		  		html+='<img style="width: 170px; height: auto" src="/images/eres_super.png">';
				html+='<br />';
		  	  	html+='<img src="/images/bad_rating_text.png">';
		  	  	$("#final_title").css("margin-bottom", "30px");
		  	  	$("#final_title").html(html);
			@endif
		  @endif
		  
		}
		setTimeout(set_message, 3000);
	  
		$("#btn1").click(function(){
		    $("#box").animate({height: "300px"});
		  });
		  $("#btn2").click(function(){
		    $("#box").animate({height: "100px"});
		  });
	  
	});
	
	</script>  

</body>
</html>