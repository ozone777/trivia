<div id="top_area">

	<div class="row">
		<div class="col">
			<div class="text-center">
				<h1>TOP 3</h1>
			</div>
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="col">
			<div class="text-center">
				<table class="table table-borderless table-condensed text-center">
				
					<tbody>
						<tr>
	     
							<td class="align-bottom">
							@if(isset($offices[1]))
							<img src="/images/top_2.png" class="img-fluid">
							<h3>{{round($offices[1]->average,2)}}</h3>
							<h4>{{$offices[1]->name}}</h4>
							@endif
							</td>
							<td class="align-bottom">
							@if(isset($offices[0]))
							<img src="/images/top_1.png" class="img-fluid">
							<h3>{{round($offices[0]->average,2)}}</h3>
							<h4>{{$offices[0]->name}}</h4>
							@endif
							</td>
							<td class="align-bottom">
							@if(isset($offices[2]))
							<img src="/images/top_3.png" class="img-fluid">
							<h3>{{round($offices[2]->average,2)}}</h3>
							<h4>{{$offices[2]->name}}</h4>
							@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
	
  	<div class="row">
  	      <div class="col-md-12">
  			  <div class="text-center">
				  <h4>Este Top es el promedio de los mejores resultados que han logrado los equipos participantes.</h4>
			</div>
		</div>
	</div>
	
  	<div class="row">
  	      <div class="col-md-12">
  			  <div class="text-center">
        
		
		  @if($final == "true")
            <a class="myButton" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Salir
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
		  @else
			<a href="/competition" class="myButton">¡A jugar!</a>
		  @endif
		
		<br /><br />
		
		</div>
	</div>


</div>