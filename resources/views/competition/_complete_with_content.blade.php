<div id = "question_area">
	
	

	<div class="row">
		<div class="col">
			<div class="text-center">
				@if($question->id < 9)
					<h1>0{{$question->id}}</h1>
				@else
					<h1>{{$question->id}}</h1>
				@endif
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col">
			<div class="text-center">
				<h2>ESCRIBE TU RESPUESTA</h2>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="text-center">
				<img src="/images/icon{{$question->id}}.png" style="max-width: 40px; margin: 10px 10px 10px 10px;">
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col">
			<div class="text-center">
				<div id="question_content">
					 <p> {!!$question->content!!}</p>
				</div>
			</div>
		</div>
	</div>

	
	<div class="row">
		<div class="col">
			<div class="text-center">
				<textarea width="100%" class="form-control" id="answer_content"></textarea>
			</div>
		</div>
	</div>
	
	<div class="row">
	
			
		<div class="col">
					<div class="text-center">
	             <button type="submit" id="send_complete_with_content" class="myButton">
	                        Validar
	             </button>
				 	</div>
				</div>
	
	</div>
	
	<div class="row">	
		<div class="col">
					<div class="text-center">
						
						<div class="pages_info">{{$question->id}} / 10</div>
	
				 	</div>
				</div>
	

</div>

