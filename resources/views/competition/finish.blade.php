<!DOCTYPE html>
<!-- saved from url=(0051)https://getbootstrap.com/docs/4.1/examples/navbars/ -->
<html lang="en"><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/prompt.js"></script><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/runScript.js"></script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Concurso Super 99</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/navbars/">

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/navbar.css" rel="stylesheet">
	<link href="/css/quiz.css" rel="stylesheet">
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   
  </head>

  <body class="green">
	
<main role="main" class="container">	
	
	<div id="alert_area" style="display:none" class="alert alert-danger" role="alert">
	  
	</div>
	
	
	<div class="container">
		<div class="row">
			<div class="col-12">
				
				<div class="text-center">
					<img src="/images/ok_10.png" class="response_image" alt="Responsive image">
					<h4 class="finish_title">¡Gracias por tu respuesta!</h4>
					<p class="response_text">Todo suma en esta nueva apuesta 
corporativa que busca convertirnos en la primera cadena de supermercados en Panamá en implementar un Programa de Integridad y Buen Gobierno.
					</p>
					
		  		  
		  		  	<a href="/final_result" class="nextButton">Ver mis resultados</a>
		  		 
					
			   </div>
			</div>
			
		</div>
	</div>
	
</main>

<script>
	
	  var audio = new Audio('/success.wav');
	
	$(document).ready(function(){
		
	  
	    audio.play();
	});
	
	
</script>

</body>


</html>