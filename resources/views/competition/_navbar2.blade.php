	<nav id="quiz_navbar" class="navbar navbar-dark pace">
		
		<div class="brand">
			
				<img id="header_image" src="/images/header.png" style="width: 210px; height: auto" alt="">
			
		</div>

		<a class="navbar-brand" href="/#"></a>
		<button id="header_menu" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExample01">
			<ul class="navbar-nav mr-auto">
		
				<li class="nav-item"><a class="nav-link" href="/register">Ingreso</a></li>
		
				<li class="nav-item">
					@if(isset($question_id))
						<a class="nav-link" href="/help?click=como_participar&question_id={{$question_id}}">¿Cómo <br /> participar?</a>
					@else
						<a class="nav-link" href="/help?click=como_participar">¿Cómo <br /> participar?</a>
					@endif
				</li>
		  
				<li class="nav-item">
					
					@if(isset($question_id))
						<a class="nav-link" href="/help?click=quienes_pueden&question_id={{$question_id}}">¿Quiénes pueden <br /> participar?</a>
					@else
						<a class="nav-link" href="/help?click=quienes_pueden">¿Quiénes pueden <br /> participar?</a>
					@endif
					
					
				</li>

				<li class="nav-item">
					
					@if(isset($question_id))
						<a class="nav-link" href="/help?click=premios&question_id={{$question_id}}">Premios</a>
					@else
						<a class="nav-link" href="/help?click=premios">Premios</a>
					@endif
					
					
				</li>
		  
				<li class="nav-item">
					
					@if(isset($question_id))
						<a class="nav-link" href="/help?click=ganadores&question_id={{$question_id}}">¿Quiénes serán <br /> los ganadores?</a>
					@else
						<a class="nav-link" href="/help?click=ganadores">¿Quiénes serán <br /> los ganadores?</a>
					@endif
					
				</li>
		  
				<li class="nav-item">
					@if(isset($question_id))
						<a class="nav-link" href="/help?click=terminos_y_condiciones&question_id={{$question_id}}">Términos y <br /> condiciones</a>
					@else
						<a class="nav-link" href="/help?click=terminos_y_condiciones">Términos y <br /> condiciones</a>
					@endif
					
				</li>
				
				<li class="nav-item">
					<a class="nav-link" href="/competition">Concurso</a>
				</li>
				
				<li class="nav-item">
					<a class="nav-link" href="/top">Ranking
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="/final_result">Puntaje
					</a>
				</li>
				
				
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
		  	   
          
			</ul>
       
		</div>
	</nav>
</div>

<script>
	
	$( "#header_menu" ).click(function() {
   		$("#quiz_navbar").toggleClass("bg_red");
   	 	$("#header_image").toggle();
	});
	
</script>
