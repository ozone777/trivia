<div id = "question_area">
	
	

	<div class="row">
		<div class="col">
			<div class="text-center">
				<h1>0{{$question->id}}</h1>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col">
			<div class="text-center">
				<h2 style="margin: 5px 5px 5px 5px">ESCRIBE TU RESPUESTA</h2>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col">
			<div class="text-center">
				<h3>Pregunta abierta <br /> !Dame un buen consejo!</h2>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="text-center">
				<img src="/images/icon{{$question->id}}.png" style="max-width: 40px; margin: 10px 10px 10px 10px;">
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col">
			<div class="text-center">
				<div id="question_content">
					 
					 <p>{!!$question->content!!}</p>
					 
				</div>
			</div>
		</div>
	</div>



	<div id="yes_or_no_area">
	    <input type="hidden" id="yes_or_no_answer_id" name="yes_or_no_answer" value="">
		
		<div class="row">
			
			
		
			<div class="col">	
				<div class="text-center">
					<p class="yes_or_no_answers yes_or_no_button" answer_id ="{{$answers[0]->id}}">Si</p>
				
					<p class="yes_or_no_answers yes_or_no_button" answer_id ="{{$answers[1]->id}}">No</p>
				</div>
			</div>
			
			
		
		</div>
	</div>
	
	
	<div class="row">
		<div class="col">
			<div class="text-center">
				<p><strong>¿Por qué?</strong></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col">
			<div class="text-center">
				<textarea rows="4" width="100%" class="form-control" id="yes_or_no_answer_content"></textarea>
			</div>
		</div>
	</div>
	
	<div class="row">	
		<div class="col">
					<div class="text-center">
	             <button type="submit" id="send_yes_or_answer" class="myButton">
	                        Validar
	             </button>
				 	</div>
				</div>
	
	</div>
	
	<div class="row">	
		<div class="col">
					<div class="text-center">
						
						<div class="pages_info">{{$question->id}} / 10</div>
	
				 	</div>
				</div>
	
	</div>

</div>

