<!DOCTYPE html>
<!-- saved from url=(0051)https://getbootstrap.com/docs/4.1/examples/navbars/ -->
<html lang="en"><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/prompt.js"></script><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/runScript.js"></script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Concurso Super 99</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/navbar.css" rel="stylesheet">
	<link href="/css/quiz.css" rel="stylesheet">
	
	<!-- Latest compiled and minified CSS -->

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
  </head>

  <body cz-shortcut-listen="true">
	  
	  

   	@include('competition/_navbar2')
    
	<main role="main" class="container">
	
		
		
		@include('competition/_'.$question->type)
		

    </main>

   

  <script>
		  
	  function loading(){
		  $(".alert").remove();
		  $("#question_area").hide();
		  $("#quiz_navbar").hide();
		  $('body').addClass("blue");
	  	  $('body').append('<div class="loading-square"><div class="lds-hourglass"></div></div>');
	  }
	  
  		
	  
	  function ok_response(question){
		  question_id = question.id;
		  next_question_id = question_id+1;
	  	  $(".loading-square").remove();
		  $('body').addClass("green");
		  html ="";
		  html+='<div class="container">';
		  html+='<div class="row">';
		  html+='<div class="col-12">';
		  html+='<div class="text-center">';
		  html+='<img src="/images/ok_'+question_id+'.png" class="response_image" alt="Responsive image">';
		  html+='<p class="response_text">'+question.ok_response+'</p>';
		  html+='<a href="/competition" class="nextButton">¡Seguir!</a>';
		  html+='</form>';
		  html+='</div></div></div></div>';
		  $('body').append(html);
		  
		  var audio = new Audio('/success.wav');
		  audio.play();
		  
		  //setTimeout(function(){ window.location="/competition?question="+next_question_id; }, 7000);
	  }
	  
	  function wrong_response(question){
		  question_id = question.id;
		  next_question_id = question_id+1;
	  	  $(".loading-square").remove();
		  $('body').addClass("red");
		  html ="";
		  html+='<div class="container">';
		  html+='<div class="row">';
		  html+='<div class="col-12">';
		   html+='<div class="text-center">';
		  html+='<img src="/images/wrong_'+question_id+'.png" class="response_image" alt="Responsive image">';
		  html+='<p class="response_text">'+question.wrong_response+'</p>';
		  html+='<a href="/competition" class="nextButton">¡Seguir!</a>';
		  html+='</div></div></div></div>';
		  $('body').append(html);
		  
		  var audio = new Audio('/wrong_sound.wav');
		  audio.play();
		  
		  //setTimeout(function(){ window.location="/competition?question="+next_question_id; }, 7000);
	  }
	  
	  
  	/*
	
  	#################################
  	complete answers logic
  	#################################
  	*/
	
  	$(".complete_answers").click(function() {
  		$(this).hide();
  		if ( typeof inputs[cont] !== "undefined" && inputs[cont]) {
			//aux = $("#answer_field").val(); 
  			final_answer = final_answer+"_"+$(this).attr("answer_id");
  			inputs[cont].value =$(this).text();
  			cont++;
			
  		}	
  	});
	
  	$(".limpiarButton").click(function() {
  		cont= 0;
  		$('input[type="text"]').val('');
  		$(".complete_answers").show();
		final_answer ="";
  	});
	
  	$("#send_answer").click(function() {
  		//alert(final_answer);
		@if(isset($question))
			question_id = {{$question->id}};
		@endif
			
			if(final_answer.length != 10){
				$(".alert").remove();
				html='<div class="alert alert-danger">';
	  		  	html+='Debes contestar la pregunta</div>';
				$(".container").prepend(html);
				window.scrollTo(0, 0);
				return false;
			}
			
  		loading();
  		$.ajax({
  			url: "/send_answer",
  			dataType: 'JSON',
  			type: 'POST',
  			data: {question_id : question_id, final_answer: final_answer, _token: "{{ csrf_token() }}"},
  			success : function(result) {
				
				if(result["message"] == "protection"){
					window.location = "/competition?question="+(question_id+1);
				}
				
				if(result["question"].id == result["questions_count"]){
					window.location = "/finish";
				}
				
  				if(result["valid"] == true){
  					ok_response(result["question"]);
  				}else{
  					wrong_response(result["question"]);
  				}
				
  			}
			
  		});
		
  	});
	
  	/*
	
  	#################################
  	yes_or_no answers logic
  	#################################
  	*/
	
	$(".yes_or_no_button").click(function() {
		$(".yes_or_no_button").removeClass("yes_or_no_button_selected");
		$(this).addClass("yes_or_no_button_selected");
		$("#yes_or_no_answer_id").val($(this).attr("answer_id"));
	});
	
	$("#send_yes_or_answer").click(function() {
		//alert(final_answer);
		answer_id = $("#yes_or_no_answer_id").val();
		answer_content = $("#yes_or_no_answer_content").val();
		
		@if(isset($question))
			question_id = {{$question->id}};
		@endif
		
		if(answer_id ==""){
			$(".alert").remove();
			html='<div class="alert alert-danger">';
  		  	html+='Debes contestar la pregunta</div>';
			$(".container").prepend(html);
			window.scrollTo(0, 0);
			return false;
		}
		
		if(answer_content ==""){
			$(".alert").remove();
			html='<div class="alert alert-danger">';
  		  	html+='Debes contestar el ¿por qué? de la respuesta</div>';
			$(".container").prepend(html);
			window.scrollTo(0, 0);
			return false;
		}
		
		loading();
		$.ajax({
			url: "/send_answer",
			dataType: 'JSON',
			type: 'POST',
			data: {question_id : question_id, answer_id: answer_id, answer_content: answer_content, _token: "{{ csrf_token() }}"},
			success : function(result) {
				
				if(result["message"] == "protection"){
					window.location = "/competition?question="+(question_id+1)
				}
				
				if(result["question"].id == result["questions_count"]){
					window.location = "/finish";
				}
				
				if(result["valid"] == true){
					ok_response(result["question"]);
				}else{
					wrong_response(result["question"]);
				}
				
			}
			
		});
		
	});
	
	/*
  	#################################
  	multiple_choice answers logic
  	#################################
  	*/
	
	$(".multiple_choice_button").click(function() {
		$(".multiple_choice_button").removeClass("multiple_choice_button_selected");
		$(this).addClass("multiple_choice_button_selected");
		$("#answer_id").val($(this).attr("answer_id"));
	});
	
	
	$("#send_multiple_choice_answer").click(function() {
		//alert(final_answer);
		answer_id = $("#answer_id").val();
		
		@if(isset($question))
			question_id = {{$question->id}};
		@endif
			
			if(answer_id ==""){
				$(".alert").remove();
				html='<div class="alert alert-danger">';
	  		  	html+='Debes contestar la pregunta</div>';
				$(".container").prepend(html);
				window.scrollTo(0, 0);
				return false;
			}
		
		loading();
		$.ajax({
			url: "/send_answer",
			dataType: 'JSON',
			type: 'POST',
			data: {question_id : question_id, answer_id: answer_id, _token: "{{ csrf_token() }}"},
			success : function(result) {
				
				if(result["message"] == "protection"){
					window.location = "/competition?question="+(question_id+1)
				}
				
				if(result["question"].id == result["questions_count"]){
					window.location = "/finish";
				}
				
				if(result["valid"] == true){
					ok_response(result["question"]);
				}else{
					wrong_response(result["question"]);
				}
				
			}
			
		});
		
	});
	
	/*
  	#################################
  	multiple_choices answers logic
  	#################################
  	*/
	multiple_choices="";
	$(".multiple_choices_button").unbind().click(function() {
		//$(".multiple_choice_button").removeClass("multiple_choices_button_selected");
		$(this).addClass("multiple_choices_button_selected");
		
		if(multiple_choices ==""){
			multiple_choices = $(this).attr("answer_id");
		}else{
			multiple_choices = multiple_choices+","+$(this).attr("answer_id");
		}
		
		$("#answer_id").val(multiple_choices);
	});
	
	$("#clean_multiple_choices").click(function() {
		$(".multiple_choices_button").removeClass("multiple_choices_button_selected");
		$("#answer_id").val("");
		multiple_choices ="";
	});
	
	
	$("#send_multiple_choices_answer").click(function() {
		//alert(final_answer);
		answer_id = $("#answer_id").val();
		
		@if(isset($question))
			question_id = {{$question->id}};
		@endif
			
			if(answer_id ==""){
				$(".alert").remove();
				html='<div class="alert alert-danger">';
	  		  	html+='Debes contestar la pregunta</div>';
				$(".container").prepend(html);
				window.scrollTo(0, 0);
				return false;
			}
		
		loading();
		$.ajax({
			url: "/send_answer",
			dataType: 'JSON',
			type: 'POST',
			data: {question_id : question_id, answer_id: answer_id, _token: "{{ csrf_token() }}"},
			success : function(result) {
				
				if(result["message"] == "protection"){
					window.location = "/competition?question="+(question_id+1)
				}
				
				if(result["question"].id == result["questions_count"]){
					window.location = "/finish";
				}
				
				if(result["valid"] == true){
					ok_response(result["question"]);
				}else{
					wrong_response(result["question"]);
				}
				
			}
			
		});
		
	});
	
	/*
  	#################################
  	complete_with_content answers logic
  	#################################
  	*/
	
	$("#send_complete_with_content").click(function() {
		//alert(final_answer);
		answer_content = $("#answer_content").val();
		
		@if(isset($question))
			question_id = {{$question->id}};
		@endif
			
			if(answer_content ==""){
				$(".alert").remove();
				html='<div class="alert alert-danger">';
	  		  	html+='Debes contestar la pregunta</div>';
				$(".container").prepend(html);
				window.scrollTo(0, 0);
				return false;
			}
		
		loading();
		$.ajax({
			url: "/send_answer",
			dataType: 'JSON',
			type: 'POST',
			data: {question_id : question_id, answer_content: answer_content, _token: "{{ csrf_token() }}"},
			success : function(result) {
				
				if(result["message"] == "protection"){
					window.location = "/competition?question="+(question_id+1);
				}
				
				if(result["question"].id == result["questions_count"]){
					window.location = "/finish";
				}
				
				if(result["valid"] == true){
					ok_response(result["question"]);
				}else{
					wrong_response(result["question"]);
				}
				
			}
			
		});
		
	});
		  
  </script>

</body>
</html>