<div class="step1">
	
	
	<div class="row">
		<div class="col">
			<div class="text-center">
				
				<img src="/images/step_1_icon.png" class="cool_image">
			</div>
		</div>
	</div>
	
		<div class="row">
		      <div class="col-md-12">

			<p style="padding-bottom: 15px;">El concurso Yo decido ser Super busca retar el conocimiento de los colaboradores sobre el nuevo programa de Buen Gobierno Corporativo y Control Interno, invitándolos a superar una serie de preguntas que refuerzan los pilares del cambio corporativo.</p>
		
        <ul>
		
          <li>
		  <a class="step1_titles" id="comoparticipar_button" href="#"><h1>¿CÓMO</h1> <h2>PARTICIPAR?</h2></a>
		  
          <div style="display: none;" id="comoparticipar_div">
            
			<ul>
				
				<li><p>Regístrate con tu nombre y No empleado.<br />-<br /></p></li>
				<li><p>Selecciona el equipo donde está registrada tu sucursal o área de trabajo.<br />-<br /></p></li>
				<li><p>Responde 10 preguntas sobre el nuevo programa de Buen Gobierno Corporativo.<br />-<br /></p></li>
				<li><p>Entre más preguntas acertadas logres, mayor será el puntaje que aportes a tu equipo<br />-<br /></p></li>
				<li><p>Solo podrás participar una (1) vez en el tiempo que esté habilitado el concurso.<br />-<br /></p></li>
				<li><p>El objetivo es que participen la mayor cantidad de colaboradores por equipo.<br />-<br /></p></li>
				<li style="padding-bottom: 15px;"><p>Al finalizar, el equipo con mayor puntaje será premiado como ganador.<br />-<br /></p></li>
			</ul>
			</div>
			
		  </li>
		  
		  <li><a id="quienes_pueden_button" class="step1_titles" href="#"><h1>¿Quiénes</h1><h2>pueden participar?</h2></a>
			  
		  <div style="display: none;" id="quines_pueden_div">
		   <p style="padding-bottom: 15px;"> Este concurso está dirigido exclusivamente a los colaboradores de Super 99 de las distintas sucursales, áreas de trabajo y oficina principal de Panamá.</p>
		</div>
		  </li>
		  
		

		  <li><a id="premios_button" class="step1_titles" href="#"><h1>¡PREMIOS!</h1></a>
		  
  		  <div style="display: none;" id="premios_div">
  		     <p style="padding-bottom: 15px;"> Cada colaborador participante en el equipo ganador obtendrán un bono por $20 dólares para ser redimido en las tiendas Super 99.</p>
  		</div>
		  
		  </li>
		  
		  <li><a id="ganadores_button" class="step1_titles" href="#"><h1>¿Quiénes</h1> <h2>serán los ganadores?</h2></a>
		  
    		<div style="display: none;" id="ganadores_div">
				<ul>
				 <li><p>Los ganadores serán los colaboradores participantes en el equipo con mayor puntaje al cierre del concurso.<br />-<br /></p></li>
				 
				  <li style="padding-bottom: 15px;"><p>Antes de anunciar los ganadores, se hará una revisión de los datos registrados.</p></li>
				</ul>
    		</div>
		  
		  </li>
		  
		  <li><a class="step1_titles" href="#" id="terminos_y_condiciones_button"><h2><i>- Términos y <br /> condiciones - </i></h2></a>
		  
		 <div style="display: none;" id="terminos_y_condiciones_div">
		
		<ul>
		
		  <li><p>El concurso “Yo decido ser Super” es organizado por Super 99, con el objetivo de fortalecer e incentivar la participación de los colaboradores en el nuevo programa de Buen Gobierno Corporativo y Control Interno.<br />-<br /></p></li>
		  
		 

		  <li><p>La inscripción y participación será absolutamente gratuita.<br />-<br /></p></li>
		  
		  
		  
		  <li><p>El premio no es negociable, no se puede exigir su cambio por otro ni por efectivo.<br />-<br /></p></li>
		  
		 

		  <li><p>En la determinación de ganadores no interviene de manera alguna el azar, la suerte, ni la aleatoriedad.<br />-<br /></p></li>
		  
		 

		  <li><p>Quienes resulten ganadores autorizan a Super 99 y a quienes éste designe para la publicidad, a difundir su nombre, premios, imágenes, fotografías y/o voces si correspondiera, para los fines publicitarios sobre el cambio corporativo, sin límite de tiempo ni costo alguno.<br />-<br /></p></li>
		  
		

		  <li style="padding-bottom: 15px;"><p>La participación en el concurso implica el conocimiento y la aceptación de estas bases.</p></li>
		  
		</ul>
		  
	  	</div>
		  
		  </li>

          
        </ul>
		
		</div>
	  </div>
	  </div>
	  
	  
	  
	  
	  
  			<div class="row">
  	     	   <div class="col-md-12">
  			  <div class="text-center">
        		  
				<a href="/top" class="myButton">¡Seguir!</a>
		
		<br /><br />
		
			</div>
			</div>
	
			
	
	
</div>

<script>



	$( document ).ready(function() {
		
		@if(isset($click))
		
		@if($click =="como_participar")
		
			$( "#comoparticipar_button" ).trigger( "click" );
			
		@elseif($click =="quienes_pueden")
			
			$( "#quienes_pueden_button" ).trigger( "click" );
			
		@elseif($click =="premios")
			
			$( "#premios_button" ).trigger( "click" );
			
		@elseif($click =="ganadores")
			
			$( "#ganadores_button" ).trigger( "click" );
			
		@elseif($click =="terminos_y_condiciones")
			
			$( "#terminos_y_condiciones_button" ).trigger( "click" );
			
		@endif
			
		@endif
		
	  
	});
	
	function reset_divs(div){
		
		if(div != "comoparticipar_div"){
			$("#comoparticipar_div").hide();
		}
		
		if(div != "quines_pueden_div"){
			$("#quines_pueden_div").hide();
		}
		
		if(div != "premios_div"){
			$("#premios_div").hide();
		}
		
		if(div != "ganadores_div"){
			$("#ganadores_div").hide();
		}
		
		if(div != "comoparticipar_div"){
			$("#comoparticipar_div").hide();
		}
		
		if(div != "terminos_y_condiciones_div"){
			$("#terminos_y_condiciones_div").hide();
		}
		
	}
	
	$("#comoparticipar_button").click(function() {
		$("#comoparticipar_div").toggle();
		reset_divs("comoparticipar_div");
		$("html, body").animate({ scrollTop: ($(document).height() - 970) }, 1000);
	});
	
	$("#quienes_pueden_button").click(function() {
		
		$("#quines_pueden_div").toggle();
		reset_divs("quines_pueden_div");
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});
	
	$("#premios_button").click(function() {
		
		$("#premios_div").toggle();
		reset_divs("premios_div");
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});
	
	$("#ganadores_button").click(function() {
		
		$("#ganadores_div").toggle();
		reset_divs("ganadores_div");
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});
	
	$("#terminos_y_condiciones_button").click(function() {
		
		$("#terminos_y_condiciones_div").toggle();
		reset_divs("terminos_y_condiciones_div");
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});
	
	
</script>  