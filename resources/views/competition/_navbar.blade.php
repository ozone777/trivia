	<nav id="quiz_navbar" class="navbar navbar-dark pace">
		
		<div class="brand">
			<img id="header_image" src="/images/header.png" style="width: 261px; height: auto" alt="">
		</div>

		<a class="navbar-brand" href="/#"></a>
		<button id="header_menu" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExample01">
			<ul class="navbar-nav mr-auto">
		
				<li class="nav-item"><a class="nav-link" href="/register">Ingreso</a></li>
		
				<li class="nav-item">
					<a class="nav-link dropdown-toggle" href="https://example.com/" id="comoparticipar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">¿Cómo <br /> participar?</a>
		  
					<div class="dropdown-menu" aria-labelledby="comoparticipar">
            
						<ul>
				
							<li>Regístrate con tu nombre y No empleado.</li>
							<li>Selecciona el equipo donde está registrada tu sucursal o área de trabajo. </li>
							<li>Responde 10 preguntas sobre el nuevo programa de Buen Gobierno Corporativo.</li>
							<li>Entre más preguntas acertadas logres, mayor será el puntaje que aportes a tu equipo.</li>
							<li>Solo podrás participar una (1) vez en el tiempo que esté habilitado el concurso.</li>
							<li>El objetivo es que participen la mayor cantidad de colaboradores por equipo.</li>
							<li>Al finalizar, el equipo con mayor puntaje será premiado como ganador.</li>
				
						</ul>
			
					</div>
		  
				</li>
		  
				<li class="nav-item"><a class="nav-link dropdown-toggle" href="https://example.com/" id="quines_pueden" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">¿Quiénes pueden <br /> participar?</a>
					<div class="dropdown-menu dropdown_content" aria-labelledby="comoparticipar">
						Este concurso está dirigido exclusivamente a los colaboradores de Super 99 de las distintas sucursales, áreas de trabajo y oficina principal de Panamá.
					</div>
				</li>
		  
		

				<li class="nav-item"><a class="nav-link dropdown-toggle" href="https://example.com/" id="premios" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Premios</a>
		  
					<div class="dropdown-menu dropdown_content" aria-labelledby="premios">
						Cada colaborador participante en el equipo ganador obtendrán un bono por $20 dólares para ser redimido en las tiendas Super 99.
					</div>
		  
				</li>
		  
				<li class="nav-item"><a class="nav-link dropdown-toggle" href="https://example.com/" id="ganadores" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">¿Quiénes serán <br /> los ganadores?</a>
		  
					<div class="dropdown-menu dropdown_content" aria-labelledby="premios">
						Los ganadores serán los colaboradores participantes en el equipo con mayor puntaje al cierre del concurso.
						Antes de anunciar los ganadores, se hará una revisión de los datos registrados.
					</div>
		  
				</li>
		  
				<li class="nav-item"><a class="nav-link dropdown-toggle" href="https://example.com/" id="terminos_y_condiciones" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Términos y <br /> condiciones</a>
		  
					<div class="dropdown-menu dropdown_content" aria-labelledby="terminos_y_condiciones">
		
						<ul>
		
							<li>El concurso “Yo decido ser Super” es organizado por Super 99, con el objetivo de fortalecer e incentivar la participación de los colaboradores en el nuevo programa de Buen Gobierno Corporativo y Control Interno.</li>

							<li>La inscripción y participación será absolutamente gratuita.</li>
		  
							<li>El premio no es negociable, no se puede exigir su cambio por otro ni por efectivo.</li>

							<li>En la determinación de ganadores no interviene de manera alguna el azar, la suerte, ni la aleatoriedad.</li>

							<li>Quienes resulten ganadores autorizan a Super 99 y a quienes éste designe para la publicidad, a difundir su nombre, premios, imágenes, fotografías y/o voces si correspondiera, para los fines publicitarios sobre el cambio corporativo, sin límite de tiempo ni costo alguno.</li>

							<li>La participación en el concurso implica el conocimiento y la aceptación de estas bases.</li>
		  
						</ul>
		  
					</div>
		  
				</li>
				
				<li class="nav-item"><a class="nav-link" href="/competition">Concurso</a></li>
				
				<li class="nav-item"><a class="nav-link" href="/competition?view=top">Ranking</a></li>

				<li class="nav-item"><a class="nav-link" href="">Puntaje</a></li>
		  
          
			</ul>
       
		</div>
	</nav>
</div>