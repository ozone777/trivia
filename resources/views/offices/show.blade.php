@extends('layout')
@section('header')
<div class="page-header">
        <h1>Offices / Show #{{$office->id}}</h1>
        <form action="{{ route('offices.destroy', $office->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('offices.edit', $office->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')


    <div class="row">
        <div class="col-md-6">
			
            <div class="form-group">
                 <label for="members">MEMBERS</label>
                 <h1>{{$office->members}}</h1>
            </div>
                <div class="form-group">
                 <label for="name">NAME</label>
                 <h1>{{$office->name}}</h1>
            </div>
		</div>
		
        <div class="col-md-6">
			
            <div class="form-group">
                 <label for="members">ANSWERED</label>
				 <h1>{{$office->get_rating_info("answered")}}</h1>
            </div>
			
            <div class="form-group">
                 <label for="members">AVERAGE</label>
				 <h1>{{$office->get_rating_info("average")}}</h1>
            </div>
			
          
		</div>
		
	</div>


    <div class="row">
        <div class="col-md-12">

		            @if($users->count())
		                <table class="table table-condensed table-striped">
		                    <thead>
		                        <tr>
		                            <th>EMPLOYEE NUMBER</th> 
		                       	 	<th>NAME</th>
								 <th>OFFICE</th>
								 <th>RATING</th>
		                         <th>DETAILS</th>
		                        </tr>
		                    </thead>

		                    <tbody>
		                        @foreach($users as $user)
		                            <tr>
		                                <td>{{$user->employee_number}}</td>
		                   			 	<td>{{$user->name}}</td>
										<td>@if(isset($user->office)){{$user->office->name}}@endif</td>
										<td>{{$user->get_test_rating()}}</td>	
		                                <td><a class="btn btn-xs btn-primary" href="/user_detail/{{$user->id}}">show</a></td>
		                            </tr>
		                        @endforeach
		                    </tbody>
		                </table>
		             
		            @else
		                <h3 class="text-center alert alert-info">Empty!</h3>
		            @endif

		    

            <a class="btn btn-link" href="{{ route('offices.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection