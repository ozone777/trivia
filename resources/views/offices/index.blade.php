@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Offices
            <a class="btn btn-success pull-right" href="{{ route('offices.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')

    <div class="row">
		<form action="/search_office" method="GET">
			
       	 	<div class="col-md-6">
				<input type="text" id="param-field" name="param" class="form-control" placeholder="Search by office"/>
			</div>
		
        	<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
			
			<br /><br /><br />
			
		</form>	
	</div>
	

    <div class="row">
        <div class="col-md-12">
           
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>MEMBERS</th>
							<th>ANSWERED</th>
                       	 	<th>NAME</th>
							<th>RATING</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($offices as $office)
                            <tr>
                                <td>{{$office->id}}</td>
                                <td>{{$office->members}}</td>
								<td>{{$office->answered}}</td>
                    			<td>{{$office->name}}</td>
								<td>{{$office->average}}</td>
                                <td class="text-right">
                                    
									<a class="btn btn-xs btn-primary" href="{{ route('offices.show', $office->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Show</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('offices.edit', $office->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('offices.destroy', $office->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
               

        </div>
    </div>

@endsection