<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Bootstrap 4 Responsive Layout Example</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>

<style>
	
	html,body {
	  height: 100%;
	}
	
	</style>

<body>

<div class="container-fluid d-flex h-100 flex-column">
    <!-- I want this container to stretch to the height of the parent -->
    
    <div class="row bg-primary flex-fill d-flex justify-content-start overflow-auto">
        <!-- I want this row height to filled the remaining height -->
		
		
			<div class="col-12">
				<div class="text-center">
					<div class="container-fluid">
		
						<img src="/images/bodysize3.png" style="max-height: 400px;" class="img-fluid">

					</div>
						
				</div>
			</div>
		
		
        <div class="col py-2">
            <!-- if row is not the right to stretch the remaining height, this column also fine -->
            Content Sed ut perspiciatis unde omnis iste natus error sit voluptatem..
            
            <p>Sriracha biodiesel taxidermy organic post-ironic, Intelligentsia salvia mustache 90's code editing brunch. Butcher polaroid VHS art party, hashtag Brooklyn deep v PBR narwhal sustainable mixtape 
swag wolf squid tote bag. Tote bag cronut semiotics, raw denim deep v taxidermy messenger bag. Tofu YOLO Etsy, direct trade 
ethical Odd Future jean shorts paleo. Forage Shoreditch tousled aesthetic irony, street art organic Bushwick artisan cliche semiotics ugh 
synth chillwave meditation. Shabby chic lomo plaid vinyl chambray Vice. Vice sustainable cardigan, Williamsburg master cleanse hella DIY 90's blog.</p> 

<p>Ethical Kickstarter PBR 
asymmetrical lo-fi. Dreamcatcher street art Carles, stumptown gluten-free Kickstarter artisan 
Wes Anderson wolf pug. Godard sustainable you probably haven't heard of them, vegan farm-to-table Williamsburg slow-carb 
readymade disrupt deep v. Meggings seitan Wes Anderson semiotics, cliche American Apparel whatever. Helvetica cray plaid, vegan brunch Banksy 
leggings +1 direct trade. Wayfarers codeply PBR selfies. Banh mi McSweeney's Shoreditch selfies, 
forage fingerstache food truck occupy YOLO Pitchfork fixie iPhone fanny pack art party Portland.</p>
        </div>
    </div>
</div>

</body>
</html>
