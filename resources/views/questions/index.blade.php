@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Questions
            <a class="btn btn-success pull-right" href="{{ route('questions.create') }}"><i class="glyphicon glyphicon-plus"></i> Create Question</a> <br />
			
			<a class="btn btn-success pull-right" href="{{ route('answers.create') }}"><i class="glyphicon glyphicon-plus"></i> Create Answer</a>
			
        </h1>
		
		

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
			
			
			
			
            @if($questions->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th witdth="10%">ID</th>
                            <th witdth="30%">CONTENT</th>
                        	<th witdth="10%">TYPE</th>
							<th witdth="10%">WINNING COMBINATION</th>
                            <th witdth="40%">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($questions as $question)
                            <tr>
                                <td>{{$question->id}}</td>
                                <td>{!!$question->content!!}</td>
                    			<td>{{$question->type}}</td>
								<td>{{$question->winning_combination}}</td>
                                <td class="text-right">
                                    
                                    <a class="btn btn-xs btn-warning" href="{{ route('questions.edit', $question->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('questions.destroy', $question->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
							
		                    <thead>
		                        <tr>
									<th></th>
		                            <th colspan="1">ANSWER</th>
		                            <th>INDEX</th>
									<th>VALID</th>
									<th>OPTIONS</th>
							</thead>
							
							 @foreach($question->answers()->get() as $answer)
								<tr>
									<td></td>
								    <td colspan="1">{{$answer->content}}</td>
									<td>{{$answer->index}}</td>
									<td>{{$answer->valid}}</td>
                                    <td><a class="btn btn-xs btn-warning" href="{{ route('answers.edit', $answer->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('answers.destroy', $answer->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form></td>
								 </tr>
							 @endforeach
							
							
                        @endforeach
                    </tbody>
                </table>
                {!! $questions->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection