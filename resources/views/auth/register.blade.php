<?php
	
$offices = \App\Office::orderBy('id', 'desc')->get();
$uniqid = uniqid();
	
?>

<!DOCTYPE html>
<!-- saved from url=(0051)https://getbootstrap.com/docs/4.1/examples/navbars/ -->
<html lang="en"><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/prompt.js"></script><script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/runScript.js"></script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Concurso Super 99</title>

	<link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/navbars/">

	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="/css/navbar.css" rel="stylesheet">
	<link href="/css/quiz.css" rel="stylesheet">
	
	<!-- Latest compiled and minified CSS -->

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
</head>

<style>

	@media (max-width: 576px) { 

		img{
			max-height: 370px;
			}
	}
	
    @media (min-width: 577px) { 
		img{
			max-height: 410px;
			}
    }
 
    @media (min-width: 768px) { 
    	img{
    		max-height: 510px;
    	}
     }
	
</style>

<body>
	
	
	@include('competition/_navbar2')
	
<div class="container">
		
	<div class="row">
		<div class="col-12">
			<div class="text-center">
				
		
					<img src="/images/bodysize3.png">
		
			</div>
		</div>
	</div>
	
	<form class="form-horizontal" method="POST" action="{{ route('register') }}">
	
	
				
					{{ csrf_field() }}
					<input type="hidden" id="email" name="email" value="system{{$uniqid}}@milpa.com">
			
					<input type="hidden" id="password" name="password" value="{{$uniqid}}">
					<input type="hidden" id="password_confirmation" name="password_confirmation" value="{{$uniqid}}">
			
					<div class="row">
						
						<div class="col-2">
						
						</div>
						
						<div class="col-8">
						
							<input id="name" style="box-shadow: 0 0 3px #35B4EE;" placeholder="Nombre" type="text" class="form-control register_field" name="name" value="{{ old('name') }}">
							
						
						</div>
						
						<div class="col-2">
						
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-2">
						
						</div>
						
						<div class="col-8">
						
							<input id="employee_number"  style="box-shadow: 0 0 3px #35B4EE;" placeholder="No. empleado" type="employee_number" class="form-control register_field" name="employee_number" value="{{ old('email') }}" required>
							
						
						</div>
						
						<div class="col-2">
						
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-2">
						
						</div>
						
						<div class="col-8">
						
							<select class="form-control register_field" name="office_id" style="box-shadow: 0 0 3px #35B4EE;">
								<option> 
									Sucursal / Área de trabajo
								</option>
								@foreach ($offices as $office)
								<option value="{{ $office->id }}"> 
									{{ $office->name }} 
								</option>
								@endforeach    
							</select>
						
						</div>
						
						<div class="col-2">
						
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-md-12">
							<div class="text-center">

					<button type="submit" class="myButton">
						A Jugar!
					</button>
					
							</div>
						</div>
		
					</div>
              
			
			</div>
		</div>
		
	</div>
	
	</form>
	
	
</div>



<script>
	
	$( document ).ready(function() {
	  
		
	  
	});
	
	</script>  

</body>
</html>