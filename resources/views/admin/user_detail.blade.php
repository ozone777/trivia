@extends('layout')

@section('header')
 <div class="row">
     <div class="col-md-6">
 <h3>User: {{$user->name}} </h3>
 <h3>Office: {{$user->office->name}}</h3>
 
     </div>
	 
	     <div class="col-md-6">
	 
	<h1 style="font-size: 90px"> {{$user->get_test_rating()}} </h1>
 
	     </div>
	 
 </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($results->count())
                <table class="table">
                    <thead>
                        <tr>
                            
                            <th class="col-md-2">Question</th>
                       	 	<th class="col-md-2">Valid</th>
							<th class="col-md-2">Answer_id</th>
							<th class="col-md-2">Answer_ids</th>
							<th class="col-md-2">Content</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($results as $result)
                            <tr>
                                
                                <td>{{$result->question_id}} {{$result->question->content}}</td>
								<td>{{$result->result_label()}}</td>
								<td>{{$result->answer_id}}</td>
								<td>{{$result->answer_ids}}</td>
								<td>{{$result->content}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection