@extends('layout')

@section('header')
    
@endsection

@section('content')

    <div class="row">
		<form action="/search_user" method="GET">
			
       	 	<div class="col-md-6">
				<input type="text" id="param-field" name="param" class="form-control" value="{{$param}}" placeholder="Search by user"/>
			</div>
		
        	<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
			
		</form>	
	</div>

    <div class="row">
        <div class="col-md-12">
            @if($users->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>EMPLOYEE NUMBER</th> 
                       	 	<th>NAME</th>
						 <th>OFFICE</th>
						 <th>RATING</th>
                         <th>DETAILS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->employee_number}}</td>
                   			 	<td>{{$user->name}}</td>
								<td>{{$user->office_name}}</td>
								<td>{{$user->rating}}</td>
                                <td><a class="btn btn-xs btn-primary" href="/user_detail/{{$user->id}}">show</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection