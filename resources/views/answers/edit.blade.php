@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Answers / Edit #{{$answer->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('answers.update', $answer->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

				 <label for="content-field">Questions</label>
				<select class="form-control" name="question_id">
    
				  @foreach ($questions as $question)
				    <option value="{{ $question->id }}" {{ ( $question->id == $answer->question_id) ? 'selected' : '' }}> 
				        {{ $question->content }} 
				    </option>
				  @endforeach    
				</select>
				<br />

                <div class="form-group @if($errors->has('content')) has-error @endif">
                       <label for="content-field">Content</label>
                    <textarea class="form-control" id="content-field" rows="3" name="content">{{ is_null(old("content")) ? $answer->content : old("content") }}</textarea>
                       @if($errors->has("content"))
                        <span class="help-block">{{ $errors->first("content") }}</span>
                       @endif
                    </div>
                    
                    <div class="form-group @if($errors->has('valid')) has-error @endif">
                     
					 @if($answer->valid)
						<input type="checkbox" name="valid" value="1" checked> Valid
					@else
						<input type="checkbox" name="valid" value="1"> Valid
					@endif
					<br /><br />
					
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('answers.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
