<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class OfficeTableSeeder extends Seeder {

    public function run()
    {
       
	   DB::table('offices')->delete();
	   
		DB::table('offices')->insert([
					'name' => "Oficina Principal",
		            'members' => 0,
		]);
		
		DB::table('offices')->insert([
					'name' => "ALBROOK",
		            'members' => 168,
		]);
		
		DB::table('offices')->insert([
					'name' => "CABIMA",
		            'members' => 150,
		]);
		
		DB::table('offices')->insert([
					'name' => "LOS ANDES #1",
		            'members' => 157,
		]);
		
		DB::table('offices')->insert([
					'name' => "SONÁ",
		            'members' => 148,
		]);
		
		DB::table('offices')->insert([
					'name' => "PAITILLA",
		            'members' => 129,
		]);
		
		DB::table('offices')->insert([
					'name' => "JUAN DIAZ",
		            'members' => 127,
		]);
		
		DB::table('offices')->insert([
					'name' => "LA DOÑA",
		            'members' => 127,
		]);
		
		DB::table('offices')->insert([
					'name' => "VILLA ZAITAl",
		            'members' => 130,
		]);
		
		DB::table('offices')->insert([
					'name' => "VILLA LUCRE",
		            'members' => 115,
		]);
		
		DB::table('offices')->insert([
					'name' => "PORTOBELO",
		            'members' => 115,
		]);
		
		DB::table('offices')->insert([
					'name' => "TUMBA MUERTO",
		            'members' => 116,
		]);
		
		DB::table('offices')->insert([
					'name' => "BRISAS DEL GOLF",
		            'members' => 116,
		]);
		
		DB::table('offices')->insert([
					'name' => "MAÑANITA",
		            'members' => 111,
		]);
		
		DB::table('offices')->insert([
					'name' => "VALLE HERMOSO, FARO",
		            'members' => 160,
		]);
		
		
		////
		
		DB::table('offices')->insert([
					'name' => "BALBOA, PEDREGAL",
		            'members' => 167,
		]);
		
	
		
		DB::table('offices')->insert([
					'name' => " PENONOMÉ, BETHANIA",
		            'members' => 167,
		]);
		
		
		
		DB::table('offices')->insert([
					'name' => "COLÓN SABANITAS, CHIRIQUÍ BUGABA",
		            'members' => 132,
		]);
		
		
		
		DB::table('offices')->insert([
					'name' => " CAROLINA, VACAMONTE",
		            'members' => 167,
		]);
		
		
		
		DB::table('offices')->insert([
					'name' => "CENCAL, VÍA PORRAS",
		            'members' => 175,
		]);
		
		
		
		DB::table('offices')->insert([
					'name' => "DORADO, ON DE GO",
		            'members' => 167,
		]);
		
		
		DB::table('offices')->insert([
					'name' => "CHANIS, LOS ANDES #2",
		            'members' => 160,
		]);
		
	
		
		DB::table('offices')->insert([
					'name' => "COLÓN #1, SAN FRANCISCO",
		            'members' => 167,
		]);
		
		DB::table('offices')->insert([
					'name' => "CHIRIQUÍ SAN MATEO, CARNES",
		            'members' => 159,
		]);
		
		DB::table('offices')->insert([
					'name' => "PLAZA ITALIA, UCP",
		            'members' => 157,
		]);
		
		DB::table('offices')->insert([
					'name' => "CHITRÉ, TRANSPORTE",
		            'members' => 156,
		]);
		
		DB::table('offices')->insert([
					'name' => "RIO HATO, COLON 2000",
		            'members' => 166,
		]);
		
		DB::table('offices')->insert([
					'name' => "CONDADO DEL REY, DEPÓSITO CENTRAL",
		            'members' => 151,
		]);
		
		DB::table('offices')->insert([
					'name' => "CORONADO, AGUADULCE",
		            'members' => 168,
		]);
		
		DB::table('offices')->insert([
					'name' => "COSTA DEL ESTE, LEGUMBRES",
		            'members' => 162,
		]);
		
		DB::table('offices')->insert([
					'name' => "MEGA DEPOT LOS ANDES, SANTIAGO",
		            'members' => 116,
		]);
		
		DB::table('offices')->insert([
					'name' => "COMPLEJO, PUERTO ESCONDIDO",
		            'members' => 116,
		]);
		
		DB::table('offices')->insert([
					'name' => "ERA PANAMÁ, BRISAS ARRAIJÁN",
		            'members' => 125,
		]);
		
		DB::table('offices')->insert([
					'name' => "LA SABROSITA,FRIGORÍFICO",
		            'members' => 132,
		]);
		
		DB::table('offices')->insert([
					'name' => "CHORRERA EL COCO, TALLER",
		            'members' => 128,
		]);
		
		DB::table('offices')->insert([
					'name' => "LOS PUEBLOS, MEGA DEPOT",
		            'members' => 164,
		]);
		
		DB::table('offices')->insert([
					'name' => "VISTA HERMOSA, CHIRIQUÍ COROTU",
		            'members' => 164,
		]);
		
		
		
		
		
    }

}