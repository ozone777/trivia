<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class AnswerTableSeeder extends Seeder {

    public function run()
    {
		DB::table('answers')->delete();
		
		//Question 1
		//Por lo menos debe tener una en valida todas las preguntas para que funcione la logica
		DB::table('answers')->insert([
					'question_id' => 1,
		            'content' => "transparencia y profesionalismo",
					'valid' => true,
					"index" => "0"
		]);
		
		DB::table('answers')->insert([
					'question_id' => 1,
		            'content' => "colaboradores",
					'valid' => false,
					"index" => "1"
		]);
		
		DB::table('answers')->insert([
					'question_id' => 1,
		            'content' => "estándares",
					'valid' => false,
					"index" => "2"
		]);
		
		DB::table('answers')->insert([
					'question_id' => 1,
		            'content' => "mejorando",
					'valid' => false,
					"index" => "3"
		]);
		
		DB::table('answers')->insert([
					'question_id' => 1,
		            'content' => "seguir creciendo",
					'valid' => false,
					"index" => "4"
		]);
		
		//Question 2
		DB::table('answers')->insert([
					'question_id' => 2,
		            'content' => "yes",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 2,
		            'content' => "no",
					'valid' => true,
		]);
		
		
		//Question 3
		DB::table('answers')->insert([
					'question_id' => 3,
		            'content' => "Promover la integridad a todo nivel de la empresa",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 3,
		            'content' => "Promover un ambiente de trabajo sin acoso ni discriminación",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 3,
		            'content' => "Denunciar cualquier acto irregular que afecte a los trabajadores o la empresa",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 3,
		            'content' => "Todas las anteriores",
					'valid' => true,
		]);
		
		
		//Question 4
		DB::table('answers')->insert([
					'question_id' => 4,
		            'content' => "Le dirías a tu amigo que ante la situación económica en la que está, acepte por sólo esta ocasión el dinero que le ofrecen.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 4,
		            'content' => "Le dices a tu amigo que te dé una comisión del dinero que le entregarán para no denunciarlo.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 4,
		            'content' => "Le aconsejaría que no acepte el dinero y que denuncie el potencial proveedor ante la Línea Ética, pues en todo momento priman los intereses de la familia Super 99. ",
					'valid' => true,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 4,
		            'content' => "Te quedarías callado.",
					'valid' => false,
		]);
		
		//Question 5
		DB::table('answers')->insert([
					'question_id' => 5,
		            'content' => "Lo felicitaría por su gran compromiso con la arquitectura de gobierno corporativo de Super 99. ",
					'valid' => true,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 5,
		            'content' => "Le diría que fue muy torpe dejar pasar una gran oportunidad para obtener un ingreso adicional.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 5,
		            'content' => "No le diría nada.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 5,
		            'content' => "Ninguna de las anteriores.",
					'valid' => false,
		]);
		
		
		//Question 6
		DB::table('answers')->insert([
					'question_id' => 6,
		            'content' => "Te quedarías callado.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 6,
		            'content' => "Accederías a dar el soborno solicitado.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 6,
		            'content' => "No entregarías el soborno, reportarías la situación en la Línea Ética y pondrías la respectivadenuncia ante las autoridades competentes.",
					'valid' => true,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 6,
		            'content' => "Ninguna de las anteriores.",
					'valid' => false,
		]);
		
		
		//Question 7
		DB::table('answers')->insert([
					'question_id' => 7,
		            'content' => "Te quedarías callado.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 7,
		            'content' => "Le dirías a tu jefe que te dé un porcentaje del dinero para no denunciarlo.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 7,
		            'content' => "Lo felicitarías por lograr el cierre de un negocio 
que le dará muchos ingresos al Super 99.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 7,
		            'content' => "Lo denunciaría a través de la Línea Ética, pues es un acto que va en contravía de los principios que orientan la arquitectura de Buen Gobierno Corporativo.",
					'valid' => true,
		]);
		
		
		//Question 8
		DB::table('answers')->insert([
					'question_id' => 8,
		            'content' => "Le dirías que denuncie la situación ante la Línea Ética por ser un caso evidente de acoso sexual y laboral, que va en contravía de las disposiciones del código de ética de la empresa.",
					'valid' => true,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 8,
		            'content' => "Te quedarías callado.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 8,
		            'content' => "Le dirías a Andrea que acceda la invitación, pues su trabajo podría estar en riesgo.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 8,
		            'content' => "Todas las anteriores.",
					'valid' => false,
		]);
		
		//Question 9
		DB::table('answers')->insert([
					'question_id' => 9,
		            'content' => "Después de la segunda vez de realizar el procedimiento, te decidirías a tomar imágenes de los sobres que le son entregados y aborda al Gerente buscando que le retribuyan el silencio por lo que está haciendo.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 9,
		            'content' => "Guardarías silencio de lo observado y visto, a pesar de saber que se trata de una mala práctica.",
					'valid' => false,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 9,
		            'content' => "Cada vez que fuera requerido para este trabajo, le sacarías imágenes de los sobres con el celular y vas organizando las pruebas sobre esta mala práctica.",
					'valid' => true,
		]);
		
		DB::table('answers')->insert([
					'question_id' => 9,
		            'content' => "PresentarÍas una denuncia ante la Línea Ética.",
					'valid' => true,
		]);
		
		
		
		
    }

}