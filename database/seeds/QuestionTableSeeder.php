<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class QuestionTableSeeder extends Seeder {

    public function run()
    {
		
		//php artisan db:seed --class=QuestionTableSeeder
		
        DB::table('questions')->delete();
		
		DB::table('questions')->insert([
					'id' => 1,
		            'content' => "Esta nueva estructura significa que la compañía está -- sus prácticas internas, apuntando a -- de -- que impacten de forma positiva con todos sus grupos de interés, además de generar una confianza en el mercado que les permita -- . Con esta nueva estructura, la compañía se fortalecerá para seguir brindando un mejor servicio, y asegurar las mejores condiciones para todos sus --.",
		            'type' => "complete",
					"winning_combination" => "_3_2_0_4_1",
					"ok_response" => "Reconoces que estamos mejorando y que todos hacemos parte del cambio. La estructura de Gobierno Corporativo y Control Interno se propone transformar nuestro ADN hacia los más altos estándares de integridad y ética. Ahora es tu misión seguir fortaleciendo estas nuevas prácticas empresariales. ",
					"wrong_response" => "<strong>¡Es hora de despertarte y entender qué significa la nueva estructura de Gobierno Corporativo y Control Interno!</strong> Este cambio de filosofía se propone transformar nuestro ADN hacia los más altos estándares de integridad y ética, en miras de lograr que Super 99 sea líder en la región en la implementación de buenas prácticas. <strong>!Anímate a ser parte del cambio!</strong>",
					
		]);
		
		DB::table('questions')->insert([
					'id' => 2,
		            'content' => "Un proveedor me ha ofrecido dos entradas para un evento deportivo por un valor de USD 50 cada una. El proveedor no podrá asistir al evento conmigo, por ello, me propuso que lleve a un acompañante. <strong>¿Puedo aceptar las entradas?</strong>",
		            'type' => "yes_or_no",
					"ok_response" => "De acuerdo a la política de regalos, donaciones, viáticos y pagos especiales de Super 99, está prohibido recibir cualquier tipo de regalos por parte de cualquier persona externa (proveedor, cliente, contraparte). <strong>¡Así que nada de entradas para un evento deportivo!</strong>",
					"wrong_response" => "Que un regalo no te haga dudar de los principios y valores de la familia Super 99. Así que nada de entradas para un evento deportivo. De acuerdo con la política de regalos, donaciones, viáticos y pagos especiales de Super 99, que hace parte del manual y programa de cumplimiento anticorrupción, está prohibido recibir cualquier tipo de regalos por parte de cualquier persona externa (proveedor, cliente, contraparte).",
					
		]);
		
		DB::table('questions')->insert([
					'id' => 3,
		            'content' => "<strong>¿Cuáles son las responsabilidades de los colaboradores con el nuevo programa de Buen Gobierno Corporativo?</strong>",
		            'type' => "multiple_choice",
					"ok_response" => "Eres una persona que conoce sus responsabilidades, ahora no dudes en conocer también tus derechos personales y profesionales dentro de esta gran familia.",
					"wrong_response" => "<strong>¡Juega vivo, porque todas las respuestas son correctas! </strong>Este cambio corporativo va con toda y esperamos que tú conozcas todos tus derechos y responsabilidades para que juntos logremos ser líderes en las mejores prácticas de anticorrupción e integridad corporativa de la región. ",
		]);
		
		DB::table('questions')->insert([
					'id' => 4,
		            'content' => "Un amigo que trabaja en Super 99 te comenta que días pasados un potencial proveedor le ofreció una gran suma de dinero para ayudarle a obtener un contrato  con Super 99. Actualmente tu amigo necesita dinero puesto que debe pagar la universidad de sus 3 hijos. Él acude a ti en busca de un consejo. Ante esta situación, <strong>¿cómo procederías?</strong>",
		            'type' => "multiple_choice",
					"ok_response" => "Estás super sincronizado con el nuevo ADN corporativo. En Super 99 creemos que el cambio es posible. Es posible renacer, recomenzar y seguir dando lo mejor de nosotros a la sociedad panameña. Por eso apostamos por una nueva filosofía corporativa inspirada por los más altos 
valores de ética, transparencia y profesionalismo.",
					"wrong_response" => "Ni quedarnos callados, ni aceptar, ni pedir sobornos es una opción. Conoce más sobre el nuevo Programa de Gobierno Corporativo, sobre la Línea Ética que habilitamos para tí y y anímate a mejorar diariamente. ",
		]);
		
		DB::table('questions')->insert([
					'id' => 5,
		            'content' => "Un amigo que trabaja en Super 99 te cuenta que una cadena de supermercados, que tiene una competencia muy fuerte con Super 99, le ofreció en días pasados una gran suma de dinero para obtener información reservada y confidencial de la empresa. Tu amigo te cuenta que él en su momento decidió no aceptar el dinero, no entregó la información e informó de esta irregularidad ante las directivas de la empresa. 
Ante esta situación, <strong>¿cómo procederías?</strong>",
		            'type' => "multiple_choice",
					"ok_response" => "De acuerdo contigo, ¡hay que felicitar las buenas acciones!, sobre todo aquellas que nos permiten crecer como empresa y como familia. Nos alegra que hagas parte de la familia Super 99, porque con tus acciones nos ayudas a cada día ser mejores.",
					"wrong_response" => "Es momento de dejar el arroz con mango atrás, abrir los ojos y darte cuenta que algo diferente está pasando. Super 99 está apostando por una nueva filosofía corporativa inspirada por los más altos valores de integridad y ética, y como miembro de esta familia debes ayudarnos a lograr este nuevo reto.",
		]);
		
		DB::table('questions')->insert([
					'id' => 6,
		            'content' => "Super 99 necesita obtener un permiso para poner en 
funcionamiento una nueva tienda que está construyendo, te delegan realizar este trámite ante la respectiva 
autoridad pública. Cuando estás en las oficinas de esta institución, el funcionario que te atiende te dice que si quieres obtener este permiso, Super 99 debe reconocer una “comisión especial de servicios” para lograr obtener el permiso de forma oportuna. Ante el soborno que te ha solicitado el funcionario, <strong>¿cómo reaccionarías?</strong>",
		            'type' => "multiple_choice",
					"ok_response" => "Totalmente de acuerdo contigo, hay que tomar cartas en el asunto y decirle NO al soborno. Ahora Super 99 cuenta con la Línea Ética para investigar este tipo de situaciones, asegurando tu anonimato, confidencialidad, seguridad y que no habrá represalias por tu denuncia.",
					"wrong_response" => "Recuerda que ahora está en funcionamiento la Línea Ética, una herramienta para informar sobre eventos, señales de alerta o potenciales situaciones relacionadas con fraude, sobornos, malas prácticas, corrupción, lavado de activos y/o cualquier otra situación irregular que afecte los intereses de la organización y sus colaboradores. ¡Así que es momento de utilizarla!",
		]);
		
		DB::table('questions')->insert([
					'id' => 7,
		            'content' => "Super 99 logra cerrar un negocio importante con un 
cliente, el cual generará grandes ingresos. Pero has 
escuchado que para esto, tu jefe directo entregó un soborno a un empleado del cliente. Días después de cerrado el negocio, escuchas a tu jefe hablando con una persona llamada Carlos, quien es el director de compras del cliente, y están acordando el lugar para entregar un “reconocimiento” por los buenos oficios para el cierre del negocio. Ante esta situación, <strong>¿cómo reaccionarías?</strong>",
		            'type' => "multiple_choice",
					"ok_response" => "¿Cierto que ya conoces la Línea Ética? 
<strong>¡Porque estás super sincronizado con esta apuesta! </strong>Felicitaciones por tu respuesta, se ve que te la estás jugando toda por el Buen Gobierno Corporativo.",
					"wrong_response" => "Debes tener cuidado de no alentar ni involucrarte en malas prácticas que afecten a Super 99. Ante este tipo de situaciones no dudes en denunciar ante la Línea Ética, no hay ningún riesgo para tu integridad personal si presentas un caso de soborno, fraude o corrupción.",
		]);
		
		DB::table('questions')->insert([
					'id' => 8,
		            'content' => "Andrea trabaja en Super 99 y es muy amiga tuya. Mientras almuerzan, Andrea te cuenta que en días pasados el jefe de ella la invitó a su casa a pasar una “buena velada como amigos”. Como Andrea no aceptó, el jefe la ha estado acosando laboralmente y la ha cargado de trabajo de forma innecesaria. Además, el jefe de Andrea en reiteradas oportunidades le dice que la carga de trabajo puede bajar, pero que todo depende de que ella acepte la invitación que le hizo. Ante esta situación, <strong>¿cómo reaccionarías?</strong>",
		            'type' => "multiple_choice",
					"ok_response" => "Tienes claro que todo colaborador tiene derechos que deben ser respetados y es un compromiso de todos aplicar los valores y principios que propone el Buen Gobierno Corporativo.",
					"wrong_response" => "El Buen Gobierno Corporativo establece que todos los colaboradores tienen derecho a ser respetados en su dignidad, por lo que cualquier acto de acoso e intimidación es inaceptable. ¡Es momento de denunciar y llevar el nuevo ADN empresarial a nuestras prácticas diarias!",
		]);
		
		DB::table('questions')->insert([
					'id' => 9,
		            'content' => "Un colaborador de Super 99, que en calidad de mensajero, 
conductor o servicios varios… continuamente es requerido para que se traslade a la residencia de un accionista, y este le entrega grandes sumas de dinero (Dólares), en efectivo para que sean trasladados a las oficinas centrales, y 
entregados a la gerencia. <strong>¿Qué harías tú en esta situación?</strong>",
		            'type' => "multiple_choices",
					"ok_response" => "Uf, lograste una difícil, ¡felicitaciones! Cuando presentamos una denuncia ante la Línea Ética, también tenemos la posibilidad de anexar evidencia que ayude al proceso de investigación del Oficial de Cumplimiento.",
					"wrong_response" => "Sabemos que era una difícil, pues hay varias respuestas correctas. Pero es que queríamos recordarte que cuando presentas una denuncia ante la Línea Ética también tienes la posibilidad de anexar evidencia que ayude al proceso de investigación del Oficial de Cumplimiento. <strong>¡Ya lo sabes para la próxima!</strong>",
		]);
		
		DB::table('questions')->insert([
					'id' => 10,
					'content' => "<strong>¿Cómo crees que este nuevo programa de Buen Gobierno Corporativo y Control Interno beneficiará a Super 99 y te beneficiará a ti como colaborador?</strong>",
		            'type' => "complete_with_content",
					"ok_response" => "",
					"wrong_response" => "",
		]);
		
    }

}