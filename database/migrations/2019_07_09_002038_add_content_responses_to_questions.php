<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentResponsesToQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
  	  Schema::table('questions', function(Blueprint $table) {

  		$table->text('ok_response')->nullable();
		$table->text('ok_response_strong')->nullable();
		
  		$table->text('wrong_response')->nullable();
		$table->text('wrong_response_strong')->nullable();

       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
